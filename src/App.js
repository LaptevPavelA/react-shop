import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
//import Start from './components/Start';
//import LoginForm from './components/LoginForm';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/js/bootstrap.min.js';
import Router from './router/Router';
//import createBrowserHistory from 'history/createBrowserHistory';

//const history = createBrowserHistory();

class App extends Component {
  render() {
    return (
      <div className="container">        
        <Router />
      </div>
    );
  }
}

export default App;
