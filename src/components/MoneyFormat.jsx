import React from 'react';
import NumberFormat from "react-number-format";

const MoneyFormat = ({ value }) => {
    return <NumberFormat
        value={value}
        displayType={'text'}
        thousandSeparator={true}
        prefix={'$'}
    />
}

export default MoneyFormat;