import React, { Component } from 'react';
import { Row } from 'react-bootstrap';
import MoneyFormat from './MoneyFormat';





class Item extends Component {
render() {
    return (
        <div className="col-md-12 col-sm-12 col-12 panel panel-default"> 
               <hr/>
                <Row>


                <div className="col-12 col-sm-4 col-md-4 col-lg-4 ">
                    <div className="product-image panel panel-default panel-body" align="center">
                    <img src={require('../data/images/' + this.props.item.image)} alt="..." className="img-responsive center-block" width ="180" height="0"/>
                    </div>
                </div>
                        
                <div className="col-12 col-sm-8 col-md-8 col-lg-8 " align="center" >
                    <div className="panel-body">
                    <Row className="panel-body">

                        <div className="col-4 col-sm-4 col-md-4 col-lg-4">
                            <dl>
                            <dt>Name</dt>
                            <dd>{this.props.item.name}</dd>
                            </dl>
                        </div>

                        <div className="col-4 col-sm-4 col-md-4 col-lg-4">
                            <dl>
                                <dt>Issue Date</dt>
                                <dd>{this.props.item.issue_date}</dd>
                            </dl>
                        </div>

                        <div className="col-4 col-sm-4 col-md-4 col-lg-4">
                            <dl>
                            <dt>Price</dt>
                            <dd><MoneyFormat value ={this.props.item.price}/></dd>
                            </dl>
                        </div>
                    </Row>
                    

                    <Row className="panel-body">
                        <div className="col-4 col-sm-4 col-md-4 col-lg-4">
                            <dl>
                                <dt>Color</dt>
                                <dd>{this.props.item.color}</dd>
                            </dl>
                        </div>

                        <div className="col-4 col-sm-4 col-md-4 col-lg-4">
                            <dl>
                                <dt>In stock</dt>
                                <dd>{this.props.item.in_stock ? "in stock" : "missing"}</dd>
                            </dl>
                        </div>

                        <div className="col-4 col-sm-4 col-md-4 col-lg-4">
                            <button 
                                className="btn btn-success" 
                                onClick={()=>this.props.addItem(this.props.item)}>
                                Order
                            </button>
                        </div>
                    </Row>
                    
                    </div>    
                </div>


                </Row>
            </div>
        );
    }
}



export default Item;