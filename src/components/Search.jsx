import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import { Row } from 'react-bootstrap';
import Item from '../components/Item';
import items from '../data/items.json';
import DatePicker, { registerLocale } from "react-datepicker";
import ru from 'date-fns/locale/ru'
import "react-datepicker/dist/react-datepicker.css";
import MaskedInput from 'react-maskedinput';
import ScrollIntoView from 'react-scroll-into-view'
// import moment from 'moment'

const colors = [
  'Red', 'White', 'Black', 'Blue', 'Yellow', 'Green'
];
class Search extends Component {
  constructor(props){
    super (props);
    this.state = {
                  filters: {
                    dateFrom: '',
                    dateTo: '',
                    inStockOnly: false,
                    priceFrom: '',
                    priceTo: '',
                    color: ''
                },
                itemsView: 5,
                itemsViewCount: 5

    }
  }  
    onFilter = (name, value = null) => e => {
      let val;
      if (e === null){
          this.setState({ filters: { ...this.state.filters, [name]: null } });  
      } else if (e.target !== undefined) {
          val = value !== null ? value : e.target.value;
      } else {
          val = e.value === undefined ? e : e.value;
      }
      this.setState({ filters: { ...this.state.filters, [name]: val }, itemsViewCount: this.state.itemsView});
      
    }

    onMoreItems = () => {
      this.setState(prevState => ({itemsViewCount: prevState.itemsViewCount + this.state.itemsView}));
    }

    onBackItems = () => {
      this.setState(prevState => ({itemsViewCount: prevState.itemsViewCount - this.state.itemsView}))
    }

    getItemsFiltered = () => {
      const { filters } = this.state;
      //let itemsPage = items.slice(0, this.state.itemsView)
      return items.filter(item => {
          if (filters.priceFrom !== '' && item.price < filters.priceFrom) {
              return false;
          }
          if (filters.priceTo !== '' && item.price > filters.priceTo) {
              return false;
          }
          if (filters.color !== '' && item.color !== filters.color) {
              return false;
          }
          if (filters.inStockOnly === true && item.in_stock !== filters.inStockOnly) {
              return false;
          }
  
          const issue_date = new Date(item.issue_date);   
          if (filters.dateFrom !== '' && filters.dateFrom > issue_date) {
            return false;
          }
          if (filters.dateTo !== '' && filters.dateTo < issue_date) {
              return false;
          }
            return true;
      });
  };

  render() {
    registerLocale('ru', ru);
    const itemsList = this.getItemsFiltered();
    const { itemsView } = this.state;
    const { itemsViewCount } = this.state;
    const itemsListPage = itemsList.slice(itemsViewCount - itemsView, itemsViewCount)
    return (
      <div className="col-md-12 col-sm-12 col-12 panel panel-default">
        <hr/>
        <form> 

          <Row>
            <div className="col-12 col-sm-4 col-md-4 col-lg-4">
                <dl>
                    <dt>Price from</dt>
                    <dd>
                      <NumberFormat
                        className="form-control"
                        value={this.state.filters.priceFrom}
                        onValueChange={this.onFilter('priceFrom')}
                        placeholder="Price From"
                        thousandSeparator={true} 
                        prefix={'$'}
                        
                      />

                    </dd>
                </dl>
            </div>

            <div className="col-12 col-sm-4 col-md-4 col-lg-4">
                <dl>
                <dt>Price to</dt>
                <dd>
                  <NumberFormat
                    className="form-control"
                    value={this.state.filters.priceTo}
                    onValueChange={this.onFilter('priceTo')}
                    placeholder="Price To"
                    thousandSeparator={true} 
                    prefix={'$'}
                        
                  />
                </dd>
                </dl>
            </div>

            <div className="col-12 col-sm-4 col-md-4 col-lg-4">
                <dl>
                <dt>Color</dt>
                <dd><select
                     name = "color" 
                     className="custom-select"
                     onChange={this.onFilter('color')}>
                          <option></option>
                          {colors.map((name, i) => <option key={i} value={name}>{name}</option>)}
                    </select>
                </dd>
                </dl>
            </div> 
          </Row>
            
          <Row>
          <div className="col-12 col-sm-4 col-md-4 col-lg-4">
                <dl>
                    <dt>Date from</dt>
                    <dd>
                      <DatePicker
                        locale='ru'
                        className="form-control"
                        selected={this.state.filters.dateFrom
                          ? this.state.filters.dateFrom
                          : null}
                        onChange={this.onFilter('dateFrom')}                        
                        dateFormat="MM-dd-yyyy"
                        customInput={<MaskedInput size="8" mask="11-11-1111"/>}
                        popperPlacement="bottom-start"
                        peekNextMonth={true}
                        showMonthDropdown={true}
                        showYearDropdown={true}
                        dropdownMode="select"
                        />
                    </dd>
                </dl>
            </div>

            <div className="col-12 col-sm-4 col-md-4 col-lg-4">
                <dl>
                <dt>Date to</dt>
                <dd>
                    <DatePicker
                        locale='ru'
                        className="form-control"
                        selected={this.state.filters.dateTo
                          ? this.state.filters.dateTo
                          : null}
                        onChange={this.onFilter('dateTo')}                        
                        dateFormat="MM-dd-yyyy"
                        customInput={<MaskedInput size="8" mask="11-11-1111"/>}
                        popperPlacement="bottom-start"
                        peekNextMonth={true}
                        showMonthDropdown={true}
                        showYearDropdown={true}
                        dropdownMode="select"
                        />
                </dd>
                </dl>
            </div>

            <div className="col-12 col-sm-4 col-md-4 col-lg-4">
                <dl>
                <dt>Instock</dt>
                <dd>
                  <input 
                      type="checkbox" 
                      value={this.state.filters.dateFrom}
                      onChange={this.onFilter('inStockOnly', !this.state.filters.inStockOnly)}
                  />
                </dd>
                </dl>
            </div> 
          </Row>
        
        </form>
        <div id = "Items">
        {itemsListPage.map((el) => {
          return <Item
                      key = {el.id}
                      item = {{...el}}
                      addItem={this.props.addItem}/> 
        })}
        <hr/>
        </div>
        <Row className="col-12 col-sm-12 col-md-12 col-lg-12">
            <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                {(() => { 
                          if (itemsViewCount > itemsView){
                            return <ScrollIntoView selector="#HeaderShop">
                                        <button 
                                            className="btn btn-success float-left"
                                            width="100%" 
                                            onClick={this.onBackItems}>
                                            Back
                                        </button>
                                    </ScrollIntoView>                    
                          }                                                                  
                })()}
            </div> 
            <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                {(() => {if (itemsList[(itemsViewCount)]){ 
                            return <ScrollIntoView selector="#HeaderShop">
                                        <button 
                                            className="btn btn-success float-right"
                                            width="100px" 
                                            onClick={this.onMoreItems}>
                                            More
                                        </button>
                                    </ScrollIntoView>                                
                          }                                                  
                })()}
             </div>
        </Row>
        <hr/>  
        
      </div> 
    )
  };
}


export default Search;



