import React, { Component } from 'react';
import Header from '../components/Header';
import User from '../containers/User';
import '../App.css';
import { Redirect } from 'react-router-dom';


class SignUpForm extends Component{
  constructor(props){
      super (props);
      this.state={
          auth:false,
          email:'',
          password:'',
          confirmPassword:'',
          passwordConfirmError:'',
          passwordError:'',
          emailError:'',
          passwordValid:false,
          emailValid:false,
          valid:'disabled',
          formErrors: {email: '', password: ''},
      };
  } 
  
    componentDidMount() {
        let user = User.getDB('auth');
        if (user.email !== undefined) {
            this.setState({auth: true});
        }
    }
  
    handleSubmit = (e) => {
      e.preventDefault();
      if (this.state.password === this.state.confirmPassword){
        User.setDB(this.state.email, this.state.password, 'users');
        this.setState({auth:true});
      } else {
          this.setState({confirmPassword:'',
                         password:'',
                         passwordConfirmError: 'passwords do not match',
                         valid:'disabled'});
      }
    }

    handleUserInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value}, 
                      () => { this.validateField(name, value) });
    }

    validateField = (name, value) => {
        let { passwordValid } = this.state;
        let { emailValid } = this.state;
        let { formErrors }  = this.state;

        switch(name){
            case 'email':
                if (!(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i.test(value))){
                    formErrors.email = 'Invalid email address';
                } else if (User.findUser(value, 'users')){
                    formErrors.email = 'Email used';
                }
                else {
                    formErrors.email = ''
                }

                formErrors.email === '' ? emailValid = true : emailValid = false;

            break;

            case 'password':
                if (value.length < 6) {
                    formErrors.password = 'Password must be at least 6 symbols';
                } else{
                    formErrors.password = '';
                }
                formErrors.password === '' ? passwordValid = true : passwordValid = false;

            break;
            default:
            break;
        }

    this.setState({ passwordValid:passwordValid,
                    emailValid:emailValid,
                    formErrors:formErrors}, 
                    this.valid);

    }

    valid(){
        (this.state.emailValid && this.state.passwordValid && this.state.confirmPassword !== '') ?
            this.setState ({valid:''}) :
            this.setState ({valid:'disabled'});  
    }    

    render() {
        const { auth } = this.state;
        if (auth){
            return <Redirect to='/shop'/>;
        }

        return (
            <div>
                <Header />

                <form className="form-group"
                    onSubmit={this.handleSubmit}>
                    <div role ="alert" className="alert akert-danger">
                        <small>
                        </small>      
                    </div>
                    
                    <input className="form-control"
                    name = "email"
                    type="email"
                    placeholder = "email"
                    value = {this.state.email}
                    onChange = {this.handleUserInput}
                    />

                    <div className="col-sm-10">
                        <small id="passwordHelp" className="text-danger">
                        {this.state.formErrors.email}
                        </small>      
                    </div>

                    <br/>

                    <input className="form-control"
                    name = "password"
                    type="password"
                    placeholder = "password"
                    value = {this.state.password}
                    onChange = {this.handleUserInput}
                    />

                    <div className="col-sm-10">
                        <small id="passwordHelp" className="text-danger">
                        {this.state.formErrors.password}
                        </small>      
                    </div>

                    <br/>
                    
                    <input className="form-control"
                    name = "confirmPassword"
                    type="password"
                    placeholder = "confirm password"
                    value = {this.state.confirmPassword}
                    onChange = {this.handleUserInput}
                    />

                    <div className="col-sm-10">
                        <small id="passwordHelp" className="text-danger">
                        {this.state.passwordConfirmError}
                        </small>      
                    </div>

                    <br/>

                    <button className="btn btn-success" disabled={this.state.valid}>submit</button>

                </form>
            </div>          
        );
    }
    
};




export default SignUpForm;

