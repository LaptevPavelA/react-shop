import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import MoneyFormat from './MoneyFormat';



class HeaderShop extends Component {
  constructor(props) {
    super(props);
    this.state = {auth:true};
  }

  render() {
    let cart = this.props.cart;
    let sum = 0;
    if (!this.state.auth){
      return <Redirect to='/shop'/>;
    }
    return (
      <div>

<nav className="navbar navbar-expand-lg navbar-light bg-light" id = "HeaderShop">

<div className ="navbar-header">
    <Link className="navbar-brand" to = "/shop">Shop</Link>
</div>

<div className="nav navbar-nav" id="navbarText">
    <ul className="navbar-nav mr-auto">
        <li className="nav-item dropdown">
            <button className="nav-link dropdown-toggle btn btn-link" id="navbarDropdown"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {"Cart: " + cart.items.length + " items"}
            </button>
            <div className="dropdown-menu" aria-labelledby="navbarDropdown">  
                {cart.items.length ? cart.items.map((item, i)  => {
                                sum = sum + item.price; 
                                    return <div key={i} className="dropdown-item"> 
                                                      <button className="btn btn-light btn-sm" title="Убрать" onClick={() => this.props.removeItem(i)} >&#x2718;</button>
                                                      &nbsp;
                                                      {item.name}, <MoneyFormat value={item.price} /> 
                                            </div>}) 
                                        : <button className="dropdown-item">Empty</button>}
                <div className="dropdown-divider"></div>
                <span className="dropdown-item">Total: <MoneyFormat value={sum.toFixed(2)}/></span>
            </div>
        </li>

        <li className="nav-item dropdown">
            <button className="nav-link dropdown-toggle btn btn-link"  id="navbarDropdown1"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {this.props.login}
            </button>
            
            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                
                <button className="dropdown-item btn btn-link" onClick={() => this.props.logout()}>Log Out</button>
            </div>
        </li>
    </ul>
</div>
  
</nav>      

        </div>
    )
  }
}


export default HeaderShop;

// {/* {!auth ? '' : <Search addItem={this.addItem}/>} */}

// {!auth ? <Header/> : <HeaderShop 
//   login={this.state.login} 
//   cart={this.state.cart}
//   />}
