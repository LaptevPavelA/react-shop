import React, { Component } from 'react';
import Header from '../components/Header';
import { Redirect } from 'react-router-dom';
import User from '../containers/User';

class LoginForm extends Component{
    constructor(props){
        super (props);
        this.state={
            auth:false,
            email:'',
            password:'',
            error:''
        };
    }    
    
    handleSubmit = (e) => {
        e.preventDefault();
        if (this.state.email === "" || this.state.password === ""){
            this.setState({error:'not all fields are filled',
                            password:'',
                            email:''});    
        }
        else if (User.authentication (this.state.email, this.state.password)){
            this.setState({auth:true});
        } else {
            this.setState({error:'email or password don`t found',
                            password:'',
                            email:''});
        }
    }

    handleUserInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value})
    }

    componentDidMount() {
        let user = User.getDB('auth');
        if (user.length !== 0) {
            this.setState({auth: true})
        }
    }
    
      render() {
        const { auth } = this.state;
        if (auth){
            return <Redirect to='/shop'/>;
        }

          return (
            <div><Header/>
              <form className="form-group" 
                  onSubmit={this.handleSubmit}>
                  
                  <div role ="alert" className="alert akert-danger">
                    <small id="passwordHelp" className="text-danger">
                    {this.state.error}
                    </small>      
                  </div>

                  <input className="form-control"
                  name="email"
                  type="email"
                  placeholder = "email"
                  value = {this.state.email}
                  onChange = {this.handleUserInput}
                  />

                  <br/>
  
                  <input className="form-control"
                  name="password"
                  type="password"
                  placeholder = "password"
                  value = {this.state.password}
                  onChange = {this.handleUserInput}
                  />
        

                  <br/>

                  <button className="btn btn-success">submit</button>
              </form>
            </div>            
          );
      }
      
  };

export default LoginForm;