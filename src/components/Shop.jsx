import React, { Component } from 'react';
import User from '../containers/User';
import Search from '../components/Search';
import Header from '../components/Header';
import HeaderShop from './HeaderShop';


class Shop extends Component {
  constructor(props){
    super (props);
    this.state={
        auth:false,
        email:'',
        password:'',
        error:'',
        login:'',
        cart: {
          items: []
          }};
  }

  componentDidMount() {
    let user = User.getDB('auth');
      if (user.email !== undefined) {
          this.setState({auth: true,
                        login: user.email});
      }
  }

  removeItem = i =>{
    const { items } = this.state.cart;
    items.splice(i, 1);
    return this.setState({ cart: { items: items } })
}

  addItem = (item) => {
    let { items } = this.state.cart;
    const newItems = items.concat(item);
    return this.setState({ cart: { items: newItems } })
  } 

  logout = () => {
    User.logout();
    this.setState({auth:false,               
                  });
  }

  logout = () => {
    User.logout();
    this.setState({auth:false,               
                  });
  }
  
  

  render() {
    const { auth } = this.state;
    const { cart } = this.state; 
    return (
    <div>
      {!auth ? <Header/> 
             : <div> <HeaderShop
                      logout = {this.logout}
                      auth = {auth}
                      cart = {cart}
                      removeItem = {this.removeItem}
                      login = {this.state.login}/>                 
                      <Search addItem={this.addItem}/>
                </div>}
      
    </div>  
    );
  }
}

export default Shop;




