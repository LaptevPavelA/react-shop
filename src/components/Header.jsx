import React, { Component } from 'react';
import { Link } from 'react-router-dom';


class Start extends Component {
  constructor(props) {
    super(props);
    this.state = {state:[]};
  } 

  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">

          <div className ="navbar-header">
            <Link className="navbar-brand" to = "/">Shop</Link>
          </div>  
            <div className="nav navbar-nav" id="navbarText">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Link className="nav-link" to = "/signup">SignUpForm</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to = "/login">LoginForm</Link>
                </li>
              </ul>
            </div>
            
        </nav>



        </div>
    );
  }
}


export default Start;
