import React, { Component } from 'react';
import { Route, BrowserRouter } from 'react-router-dom';
//import Header from '../components/Header';
import LoginForm from '../components/LoginForm';
import SignUpForm from '../components/SignUpForm';
import Shop from '../components/Shop';
  
class Router extends Component {
	render(){	
		return(
			<BrowserRouter>
				<div>
					<Route exact path = "/" component = { LoginForm }/>
					<Route path = "/login" component = { LoginForm }/>
					<Route path = "/signup" component = { SignUpForm }/>
					<Route path = "/shop" component = { Shop }/>

				</div>
			</BrowserRouter>
			);
	}
}



export default Router;

		

