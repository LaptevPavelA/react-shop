const User = {
  
  getDB(field) {
    const users = localStorage.getItem(field);
    return users != null ? JSON.parse(users) : [];
  },  
  
  setDB (email, password, field) {
    if (field === 'users'){
      const user = { email: email, password: password };
      const users = this.getDB(field).concat(user);
      localStorage.setItem(field, JSON.stringify(users));
      if (this.findUser(email, 'auth') === undefined) {
        this.setDB(email, password, 'auth');
      }
    } else {
      const user = { email: email, password: password };
      localStorage.setItem(field, JSON.stringify(user));
    }
  },

  findUser (email, field) {
    const users = this.getDB(field);
    return users.find(user => user.email === email);
    
  },

  logout() {
    localStorage.removeItem('auth');
  },

  authentication (email, password){
    const user = this.findUser (email, 'users');
    if (user === undefined){
      return false;
    } else {
      //return user.password === password ? true : false;
      if (user.password === password){
        this.setDB(email, password, 'auth');
        return true;
      } else{
        return false;
      }
    }
  },

  // auth (email, password){
  //   const user = { email: email, password: password };
  //   const users = this.getUsers().concat(user);
  //   localStorage.setItem('auth', JSON.stringify(users));
  // },

  // getAuth (){
  //   const users = localStorage.getItem('auth');
  //   return auth !== null ?  : false 
  // }
}

export default User;
